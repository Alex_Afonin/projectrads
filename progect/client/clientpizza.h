//---------------------------------------------------------------------------

#ifndef clientpizzaH
#define clientpizzaH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Edit.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiMain;
	TTabItem *tiOrder;
	TToolBar *ToolBar1;
	TGridPanelLayout *GridPanelLayout1;
	TImage *Image1;
	TLabel *Label1;
	TEdit *Edit1;
	TImage *Image2;
	TButton *buOrder;
	TButton *buBase;
	TLayout *Layout1;
	TButton *Button3;
	TTabItem *tiMenu;
	TEdit *Edit2;
	TEdit *Edit3;
	TLabel *Label2;
	TLabel *Label3;
	TButton *buOrdering;
	TButton *Button1;
	TLabel *Label4;
	TToolBar *ToolBar2;
	TButton *Button2;
	TLabel *Label5;
	TTabItem *TabItem1;
	TToolBar *ToolBar3;
	TButton *Button4;
	TLabel *Label6;
	void __fastcall buOrderClick(TObject *Sender);
	void __fastcall buBaseClick(TObject *Sender);
	void __fastcall Button3Click(TObject *Sender);
	void __fastcall Button2Click(TObject *Sender);
	void __fastcall Button1Click(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
